import psycopg2

from rand_utils import rand_string
import random

# Connect to the db
con = psycopg2.connect(
    host="0.0.0.0", database="test", user="postgres", password="changeme"
)

# Cursor
cur = con.cursor()

# Initialize the database tables

table_contrib_res_sql = """
CREATE TABLE public.contributor_resource
(
    resource_id text COLLATE pg_catalog."default" NOT NULL,
    contributor_id text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pkey PRIMARY KEY (resource_id, contributor_id),
    CONSTRAINT contributor_id FOREIGN KEY (contributor_id)
        REFERENCES public.contributor (uid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT resource_id FOREIGN KEY (resource_id)
        REFERENCES public.resource (uid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.contributor_resource
    OWNER to postgres;
"""

contrib_sql = """
CREATE TABLE public.contributor
(
    uid text COLLATE pg_catalog."default" NOT NULL,
    cern_id integer,
    name text COLLATE pg_catalog."default",
    CONSTRAINT contributor_pkey PRIMARY KEY (uid)
)

TABLESPACE pg_default;

ALTER TABLE public.contributor
    OWNER to postgres;
"""

resource_sql = """
CREATE TABLE public.resource
(
    uid text COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    ingestion_data json,
    CONSTRAINT resource_pkey PRIMARY KEY (uid)
)

TABLESPACE pg_default;

ALTER TABLE public.resource
    OWNER to postgres;
"""

# Create some sample records

## Resources


def create_resource():
    uid = rand_string(length=7, charset="digits")
    name = rand_string(length=10, charset="lowercase")
    ingestion_data = "{}"
    r = [uid, name, ingestion_data]
    return r


def create_contrib():
    uid = rand_string(length=7, charset="digits")
    name = rand_string(length=10, charset="lowercase")
    cern_id = rand_string(length=5, charset="digits")
    c = [uid, cern_id, name]
    return c


print("Created contributors table")
cur.execute(contrib_sql)
print("Created resources table")
cur.execute(resource_sql)
print("Created contrib_res table")
cur.execute(table_contrib_res_sql)

resources = []

for i in range(10):
    r = create_resource()
    resources.append(r)
    insert_query = f"""
    INSERT INTO public.resource(
        uid, name, ingestion_data)
        VALUES ({r[0]}, '{r[1]}', '{r[2]}');
    """
    cur.execute(insert_query)

print("Inserted resource sample records:", resources)

contributors = []

for i in range(10):
    c = create_contrib()
    contributors.append(c)
    insert_query = f"""
    INSERT INTO public.contributor(
        uid, cern_id, name)
        VALUES ({c[0]}, '{c[1]}', '{c[2]}');
    """
    cur.execute(insert_query)

print("Inserted contributors sample records:", contributors)

## Contributors

## Assign Contributors to Resources

# for each resource
for i in range(10):
    res_uid = resources[i][0]
    # assign 2 random contributors
    for j in range(2):
        n = random.randint(0, 9)
        contrib_uid = contributors[n][0]
        insert_query = f"""
        INSERT INTO public.contributor_resource(
            resource_id, contributor_id)
            VALUES ({res_uid}, {contrib_uid});
        
        """
        cur.execute(insert_query)


# commit the transcation
con.commit()

# close the cursor
cur.close()

# close the connection
con.close()
