# DM Registry

## Prerequisites

You need:

- Python3 (optionally [pipenv](https://pypi.org/project/pipenv/))
- Node + npm
- Docker (optionally [docker-compose](https://docs.docker.com/compose/))

And these packages:

```bash
# Required to build psycopg2 from source 
sudo apt install libpq-dev python3-dev
# Alternatively, install `psycopg2-binary` PyPI package instead of `psycopg2`
# (For production use you are advised to use the source distribution.)
```

### Postgres

Spin up a psql instance in a docker container:

```bash
docker run --name registry-psql -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

OR - use the provided docker compose setup to start postgresql and pg-admin:

### docker-compose postgres setup

```bash
docker-compose -d up
```

Environments variables:

- `POSTGRES_USER` the default value is **postgres**
- `POSTGRES_PASSWORD` the default value is **changeme**
- `PGADMIN_PORT` the default value is **5050**
- `PGADMIN_DEFAULT_EMAIL` the default value is **pgadmin4@pgadmin.org**
- `PGADMIN_DEFAULT_PASSWORD` the default value is **admin**

Access to postgres: 

- `localhost:5432`
- **Username:** postgres (as a default)
- **Password:** changeme (as a default)

Access to PgAdmin: 

- **URL:** [`http://localhost:5050`](http://0.0.0.0:5050)
- **Username:** pgadmin4@pgadmin.org (as a default)
- **Password:** admin (as a default)

Add a new server in PgAdmin:

- **Host name/address** `postgres`
- **Port** `5432`
- **Username** as `POSTGRES_USER`, by default: `postgres`
- **Password** as `POSTGRES_PASSWORD`, by default `changeme`

## Run

Activate a Python virtual environment and install Python dependencies:

```bash
pipenv shell
pipenv install
```

### Database

Supposing you have a working instance of postgresql running, create a `test` db and run `python initdb.py` to initialise the database.

### API

Run the server with: `python server.py`

- [`0.0.0.0:5000/api/v1/`](http://0.0.0.0:5000/api/) - API endpoint
- [`0.0.0.0:5000/api/v1/ui/`](http://0.0.0.0:5000/api/v1/ui/) - Swagger UI

### UI

To start a development server:

```bash
cd registry-ui
npm install
npm run serve
```

Frontend will be online at [`0.0.0.0:8080`](http://0.0.0.0:8080). 

`npm run build` will produce a static build.