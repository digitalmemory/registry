import random
import string


def rand_string(length=10, charset="lowercase"):
    if charset == "lowercase":
        chars = string.ascii_lowercase
    elif charset == "uppercase":
        chars = string.ascii_uppercase
    elif charset == "letters":
        chars = string.ascii_letters
    elif charset == "digits":
        chars = string.digits

    random_string = "".join(random.choice(chars) for i in range(length))

    return random_string
