import connexion
from flask_cors import CORS

options = {"swagger_ui": True}
app = connexion.App(__name__, specification_dir="openapi/", options=options)
app.add_api("registry.yml")

# add CORS support
CORS(app.app)

app.run(port=5000)
