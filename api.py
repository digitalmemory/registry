import psycopg2

# Connect to the db
con = psycopg2.connect(
    host="0.0.0.0", database="test", user="postgres", password="changeme"
)

cur = con.cursor()


def test():
    return "DONE", 200


def dbstatus():
    return con.status


def listresources():
    sql = """
    SELECT * FROM public.resource
    ORDER BY uid ASC 
    """
    cur.execute(sql)
    return cur.fetchall()


def listcontribs():
    sql = """
    SELECT * FROM public.contributor
    ORDER BY uid ASC 
    """
    cur.execute(sql)
    return cur.fetchall()


def listres_contrib(resource_id):
    sql = f"""
    SELECT *
    FROM public.contributor
    INNER JOIN public.contributor_resource ON (public.contributor_resource.contributor_id=public.contributor.uid)
    WHERE (public.contributor_resource.resource_id='{resource_id}')
    """
    cur.execute(sql)
    return cur.fetchall()


def listcontrib_res(contributor_id):
    sql = f"""
    SELECT *
    FROM public.resource
    INNER JOIN public.contributor_resource ON (public.contributor_resource.resource_id=public.resource.uid)
    WHERE (public.contributor_resource.contributor_id='{contributor_id}')
    """
    cur.execute(sql)
    return cur.fetchall()
